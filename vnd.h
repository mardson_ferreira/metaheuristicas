#ifndef VND_H
#define VND_H

#include "heuristic.h"
#include "grafo.h"
#include "vector"
#include "localsearch.h"
#include "climber.h"

class VND : public Heuristic, public Climber
{
public: 
    VND(Grafo *grafo, vector<lsmethod>& neighMethods);
    int climb(int output[], int custo);

private:
    vector<lsmethod> lsNeighbors; //vetor contendo os métodos de vizinhanças
    bool verbose = false;
    bool firstImp = true;
};

#endif // VND_H
