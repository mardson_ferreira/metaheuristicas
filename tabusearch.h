#ifndef TABUSEARCH_H
#define TABUSEARCH_H

#include "heuristic.h"
#include "grafo.h"
#include "tabulist.h"
#include "solver.h"

#define MAXGAP 3

class TabuSearch : public Heuristic
{
public:
    TabuSearch(Grafo* grafo, Solver* solver, int tenure);
    int solve(int iter, int best[]);
    int pickAFlip2(int tag[], int custo, int maxGap);
private:
    TabuList tabuList;
    int *current, *neigh, *map;
    Solver* solver;
    bool verbose = true;

    int climb(int current[], int currentCost);
    int flip2Tabu(int output[], int bestCost);


};

#endif // TABUSEARCH_H
