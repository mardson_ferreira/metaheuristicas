TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    grafo.cpp \
    heuristic.cpp \
    localsearch.cpp \
    hillclimb.cpp \
    multistart.cpp \
    greedygenerator.cpp \
    greedyadaptativegen.cpp \
    randomgenerator.cpp \
    grasp.cpp \
    vnd.cpp \
    vns.cpp \
    simulatedannealing.cpp \
    tabulist.cpp \
    tabusearch.cpp \
    ils.cpp \
    gls.cpp \
    scattersearch.cpp \
    aed.cpp \
    ga.cpp

HEADERS += \
    grafo.h \
    def.h \
    heuristic.h \
    localsearch.h \
    hillclimb.h \
    multistart.h \
    greedygenerator.h \
    greedyadaptativegen.h \
    randomgenerator.h \
    grasp.h \
    solver.h \
    vnd.h \
    climber.h \
    vns.h \
    simulatedannealing.h \
    tabulist.h \
    solution.h \
    tabusearch.h \
    ils.h \
    gls.h \
    scattersearch.h \
    aed.h \
    ga.h \
    iga.h

QMAKE_CXXFLAGS += -std=c++11
