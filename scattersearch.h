#ifndef SCATTERSEARCH_H
#define SCATTERSEARCH_H

#include "heuristic.h"
#include "grafo.h"
#include "solver.h"
#include "climber.h"
#include "solution.h"

class ScatterSearch : public Heuristic
{
public:
    ScatterSearch(Grafo *g, Solver* s, Climber* c);
    int solve(int best[], int popSize, int refSize, int tryouts);

private:
    Solver *solver;
    Climber *climber;
    int *map, *filho, *verticeA, *verticeB, *tag;
    bool *used;
    bool verbose = true;

    Solution* scatterSet( int popSize, int refSize);
    Solution cross(Solution a, Solution b);
    Solution pathRelink(Solution sol1, Solution sol2);

};

#endif // SCATTERSEARCH_H
