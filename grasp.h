#ifndef GRASP_H
#define GRASP_H

#include "grafo.h"
#include "heuristic.h"
#include "greedyadaptativegen.h"
#include "multistart.h"
#include "climber.h"

class GRASP : public Heuristic
{
private:
    int* tag;
    bool verbose= true;
    bool fip = true;
    Climber* vnd;
    MultiStart ms;
    GreedyAdaptativeGen gag;


public:
    GRASP(Grafo *grafo, int tol, bool firstImpr, Climber *vnd);
    int solve(int iter, int output[]);
};

#endif // GRASP_H
