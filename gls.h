#ifndef GLS_H
#define GLS_H

#include "heuristic.h"
#include "grafo.h"
#include "solver.h"
#include "climber.h"

class GLS : public Heuristic
{
public:
    GLS(Grafo* grafo, Solver *solver, Climber* climber);
    int solve(int best[], int iter, float alpha);

private:
    float **verticeTagPenalty;
    Solver *solver;
    Climber *climber;
    int *current;
    int *map;
    bool verbose = true;

    int climb(int current[], int currentCost, float alpha);
    float calcPenalty(int current[], float alpha);
    int flip2Tabu(int output[], float penaltyCost, int cost, float alpha);


};

#endif // GLS_H
