#include "gls.h"



GLS::GLS(Grafo *grafo, Solver *solver, Climber *climber): Heuristic(grafo), solver(solver), climber(climber)
{
    current = new int[grafo->numNodes];
    map = new int[grafo->numNodes];

    int i = {0};
    std::generate(map, map+grafo->numNodes, [&i]{ return i++; }); //inicializa map com valores de 0 até g.N-1

    verticeTagPenalty = new float*[grafo->numNodes];
    for (int i = 0 ; i < grafo->numNodes ; i++ ){
        verticeTagPenalty[i] = new float[grafo->numNodes];
        for(int j = 0; j < grafo->numNodes; j++)
            verticeTagPenalty[i][j] = 1;
    }

}

int GLS::solve(int best[], int iter, float alpha){
    int currentCost = climber->climb(current, solver->solve(current));
    int bestCost = currentCost;

    for(int i = 0; i < iter; i++){
        for(int j = 0; j < grafo->numNodes; j++){
            for(int k = 0; k < grafo->numNodes; k++){
                verticeTagPenalty[j][k] *= .5f;
            }
            verticeTagPenalty[j][current[j]]++;
        }


        currentCost = climb(current, currentCost, alpha);
        if(bestCost > currentCost){
            bestCost = currentCost;
            std::copy(current, current + grafo->numNodes, best); //copy current to best
        }

        if(verbose){
            cout << i << "\t " << currentCost << "\t " << bestCost << endl;
        }

    }

    return bestCost;
}

int GLS::climb(int current[], int currentCost, float alpha){
    float cP;
    float ncP = currentCost * calcPenalty(current, alpha);
    int c;
    int nc = currentCost;

    do{
        cP = ncP;
        c = nc;
        nc = flip2Tabu(current, cP, c, alpha);
        ncP = nc * calcPenalty(current, alpha);
    }while(ncP < cP);

    return nc;

}

int GLS::flip2Tabu(int output[], float penaltyCost, int cost, float alpha){
    random_shuffle(map, map+grafo->numNodes);
    for(int ii = 0; ii < grafo->numNodes; ii++)
    {
        for(int jj = 0; jj < grafo->numNodes; jj++)
        {
            int i = map[ii];
            int j = map[jj];
            int delta = grafo->flipDelta(i, j,output);
            Grafo::flip(i, j, output);
            float flipCost = (delta + cost) * calcPenalty(output, alpha);
            if(penaltyCost > flipCost)
            {
                return delta + cost;
            }
            Grafo::flip(i, j, output);
        }
    }
    return cost;
}

float GLS::calcPenalty(int current[], float alpha){
    int c = 0;
    for(int i = 0; i < grafo->numNodes; i++){
        c += verticeTagPenalty[i][current[i]];
    }
    return 1 + alpha * c;
}
