#ifndef AED_H
#define AED_H

#include "heuristic.h"
#include "grafo.h"
#include "climber.h"
#include "list"

class AED : public Heuristic
{
public:
    AED(Grafo* grafo, Climber* climber);
    int solve(int output[], int popSize, int eliteSize, int iter, double alpha);

private:
    double** p;
    int** freq;
    double p0;
    list<Solution> elite;


    int* map;
    Climber* climber;
    bool* saiu;
    double* pAcu;
    int* rmap;

    Solution sample(Solution &s);
    int roleta(int v, bool saiu[]);
    int roletaNormalizada(int v, bool saiuRotulo[]);
    int roleta2(int v, bool saiuRotulo[]);

};

#endif // AED_H
