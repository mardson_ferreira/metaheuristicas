#include "tabusearch.h"

TabuSearch::TabuSearch(Grafo *grafo, Solver *solver, int tenure):Heuristic(grafo), solver(solver), tabuList(tenure, grafo->numNodes)
{
    current = new int[grafo->numNodes];
    neigh = new int[grafo->numNodes];
    map = new int[grafo->numNodes];

    int i = {0};
    std::generate(map, map+grafo->numNodes, [&i]{ return i++; }); //inicializa map com valores de 0 até g.N-1

    random_shuffle(map, map+grafo->numNodes); //embaralha map

    std::copy(map, map + grafo->numNodes, current);

}


int TabuSearch::flip2Tabu(int output[], int bestCost){
    random_shuffle(map, map+grafo->numNodes);

    for(int k = 0; k < grafo->numNodes; k++){
        for(int l = k + 1; l < grafo->numNodes; l++){
            int i = map[k];
            int j = map[l];
            int delta = grafo->flipDelta(i,j,output);

            Grafo::flip(i,j,output);
            if(delta < 0 && !tabuList.isTabu(bestCost + delta, output)){
                return bestCost + delta;
            }
            Grafo::flip(i,j,output);
        }
    }

    return bestCost;
}


int TabuSearch::climb(int current[], int currentCost){

    int c;
    int nc = currentCost;
    do {
        c = nc;
        nc = flip2Tabu(current, c);
    } while (nc < c);

    return nc;
}

int TabuSearch::solve(int iter, int best[]){
    if(verbose){
        cout << "### Tabu Search started ###" << endl;
    }

    int currentCost = solver->solve(current);
    int bestCost = currentCost;
    tabuList.clear();

    for(int i = 0; i < iter; i++){
        currentCost = climb(current, currentCost);
        if(bestCost > currentCost){
            bestCost = currentCost;
            std::copy(current, current + grafo->numNodes, best); //copy current to best
            if(verbose){
                cout << "cost: " << bestCost << endl;
            }
        }

        if(!tabuList.isTabu(currentCost, current)){
            tabuList.add(current, currentCost);
            if(verbose){
                cout << "iter: " << i << "currentCost: " << currentCost << "best: " << bestCost << endl;
            }
        }

        currentCost = pickAFlip2(current, currentCost, MAXGAP);
    }

    if(verbose){
        cout << "### Tabu Search done ###" << endl;
    }

    return bestCost;
}

int TabuSearch::pickAFlip2(int tag[], int custo, int maxGap){
    random_shuffle(map, map+grafo->numNodes);

    for(int i = 1; i < grafo->numNodes; i++){
        int tag0 = tag[map[0]];
        if( abs(tag0 - tag[map[i]]) <= maxGap){
            int aux = map[i];
            map[i] = map[1];
            map[1] = aux;
            break;
        }
    }

    int delta = grafo->flipDelta( map[0], map[1], tag);
    Grafo::flip(map[0], map[1], tag);

    return custo + delta;
}
