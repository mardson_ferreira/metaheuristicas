#include "vns.h"

VNS::VNS(Grafo *g, Solver *s, Climber *c):Heuristic(g), solver(s), climber(c)
{
    neighbor = new int[g->numNodes];
    map = new int[g->numNodes];

    int i = {0};
    std::generate(map, map+grafo->numNodes, [&i]{ return i++; }); //inicializa map com valores de 0 até g.N-1

}

//pertuba uma solução com a permutação de n elementos
int VNS::pickAFlip(const int n, int tag[], const int cost){
    random_shuffle(map, map+grafo->numNodes);
    srand (time(NULL));

    int delta = grafo->subCost(tag, map, n);

    for(int i = n-1; i > 0; i--){
        int nextInt = rand() % i; // range 0 to  i
        Grafo::flip(map[i], map[nextInt], tag);
    }

    delta = grafo->subCost(tag, map, n) - delta;

    return cost + delta;
}


int VNS::solve(int current[], int maxFlip, int tryouts){
    if(verbose){
        cout << "### VNS started ###" << endl;
    }

    int currentCost = climber->climb(current, solver->solve(current));
    int j = 2, k = 0;
    while( j <= maxFlip){
        std::copy(current, current + grafo->numNodes, neighbor);
        int neighborCost = climber->climb(neighbor, pickAFlip(j, neighbor, currentCost));
        if(neighborCost < currentCost){
            std::copy(neighbor, neighbor + grafo->numNodes, current);
            currentCost = neighborCost;
            if(verbose){
                cout << "flip " << j << " cost: " << currentCost;
            }
            j = 2;
        }else{
            k++;
            if( k > tryouts){
                j++; //aumento o tamnho da vizinhança
                k = 0; //zero o número de tentativas na nova vizinhança
            }
        }

    }

    if(verbose){
        cout << "### VNS Done ###" << endl;
    }

    return currentCost;
}
