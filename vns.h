#ifndef VNS_H
#define VNS_H

#include "grafo.h"
#include "heuristic.h"
#include "solver.h"
#include "climber.h"

class VNS : public Heuristic
{
public:
    VNS(Grafo* g, Solver* s, Climber* c);
    int solve(int current[], int maxFlip, int tryouts);

private:
    int* neighbor;
    int* map;
    Solver* solver;
    Climber* climber;
    bool verbose = true;
    int pickAFlip(const int n, int tag[], const int cost);

};

#endif // VNS_H
