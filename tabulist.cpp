#include "tabulist.h"

TabuList::TabuList(int t, int size_sol)
{
    tenure = t;
    lenght_sol = size_sol;
    cout << "tamanho da solução" << lenght_sol << endl;
}


void TabuList::add(int tag[], int cost){
    Solution solution(tag, cost, lenght_sol);

    if(lista.size() > tenure){
        lista.pop_front(); //remove do início
    }

    lista.push_back(solution); //insere no fim

}

bool TabuList::isTabu(int cost, int tag[]){
    if (lista.size() == 0)
        return false;

    Solution e(tag, cost, lenght_sol);
    for(Solution s : lista){
        if(s == e)
            return true; //só é possivel por causa do operator ==
    }
    return false;
}

void TabuList::clear(){
    lista.clear();
}

int TabuList::size(){
    return lista.size();
}

int TabuList::lenghtSolution(){
    return lenght_sol;
}
