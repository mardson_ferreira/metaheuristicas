#include <iostream>
#include <vector>
#include <sys/time.h>
#include <dirent.h>

#include "grafo.h"
#include "randomgenerator.h"
#include "multistart.h"
#include "hillclimb.h"
#include "greedygenerator.h"
#include "greedyadaptativegen.h"
#include "grasp.h"
#include "vnd.h"
#include "vns.h"
#include "simulatedannealing.h"
#include "tabusearch.h"
#include "ils.h"
#include "gls.h"
#include "scattersearch.h"
#include "aed.h"
#include "ga.h"

using namespace std;

double get_wall_time(){
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

double get_cpu_time(){
    return (double) clock() / CLOCKS_PER_SEC;
}

int main(int argc, char *argv[])
{

    DIR *diretorio = 0;
    struct dirent *entrada = 0;
    unsigned char isFile = 0x8;

    diretorio = opendir(argv[1]);

    if (diretorio == 0) {
       std::cerr << "Nao foi possivel abrir diretorio." << std::endl;
       exit (1);
    }

    ofstream output("/home/mardsonferreira/metaheuristicas/resultados.txt", ofstream::out);

    output << "\t\t\t RELATÓRIO DAS INSTÂNCIAS " << endl;

    try {
            while (entrada = readdir(diretorio))
            {
                if(entrada->d_type == isFile)
                {

                    Grafo grafo(argv[1], entrada->d_name);
                    double cpu0, cpu1;

                    output << "INSTÂNCIA: " << entrada->d_name << endl;


                    int* out= new int[grafo.numNodes];

                    int i = {0};
                    std::generate(out, out+grafo.numNodes, [&i]{ return i++; });

                    srand( time( NULL ) );

                    vector<lsmethod> neighborhoodMethods;

                    LocalSearch ls(&grafo);

                    neighborhoodMethods.push_back([&ls](int output[], int bestCost, bool firstImprove) {
                        return ls.flip2(output,bestCost,firstImprove,1,10);
                    });

                    neighborhoodMethods.push_back([&ls](int output[], int bestCost, bool firstImprove) {
                        return ls.flip3(output,bestCost,firstImprove,0,3);
                    });

                    /// RandomGenerator rg(&grafo);

                    HillClimb hc(&grafo);

                    hc.findInitialPoint(out, grafo.numNodes);

                    // MultiStart ms(&grafo, false); OK!

                    /// GreedyGenerator gg(&grafo);

                    GreedyAdaptativeGen gag(&grafo, 1);

                    VND vnd(&grafo, neighborhoodMethods);

                    // GRASP grasp(&grafo,2, true, &vnd); OK!

                    // VNS vns(&grafo, &gag, &vnd); OK!

                    // SimulatedAnnealing sa(&grafo, &gag); OK!

                    // TabuSearch ts(&grafo, &gag, 1000); OK!

                    // ILS ils(&grafo, &gag, &vnd); OK!

                    // GLS gls(&grafo, &gag, &vnd); OK!

                    // ScatterSearch ss(&grafo, &gag, &vnd); OK!

                    /// AED aed(&grafo, &vnd);

                    GA ga(&grafo, 1000);

                    int result = 0;

                    cpu0 = get_cpu_time();

                    /// result = gg.solve(out);

                    /// result = gag.solve(out);

                    /// result = aed.solve(out,100, 10, 1000, .5);

                    result = ga.solve(100000, out, .5, &gag);

                    /*
                     * result = ms.solve(1000, out, &vnd, &hc);
                     * result = ils.solve(out, 2, 1000, 1.2);
                     * result = vnd.climb(out, grafo.cost(out));
                     * result = vns.solve(out, 10, 1000);
                     * result = grasp.solve(1000,out);
                     * result = sa.solve(100000, out, 100, 3);
                     * result = ts.solve(1000, out);
                     * result = gls.solve(out, 1000, .1f);
                     * result = ss.solve(out,100, 15, 1000);
                    */


                    cpu1 = get_cpu_time();

                    output << "\n CPU Time  = " << cpu1  - cpu0  << " seg" << endl;

                    output << "\n Final Cost:" << result << endl;

                    output << "\n GAP:" << grafo.calculeGap(result) << endl;

                    output << "\n Solution" << endl;
                    for(int k = 0; k < grafo.numNodes; k++)
                            output << " " << out[k];
                    output << "\n\n";

                }
            }

            output.close();

        }
        catch (string& s) {
            cerr << s << endl;
            cerr << "exception caught" << endl;
        }
        catch (...) {
            cerr << "unknown exception" << endl;
        }

    return 0;
}
