#include "aed.h"

AED::AED(Grafo *grafo, Climber *climber) : Heuristic(grafo), climber(climber)
{
    cout << "Building AED" << endl;
    p = new double*[grafo->numNodes];
    for(int i = 0; i < grafo->numNodes; i++)
        p[i] = new double[grafo->numNodes];

    freq = new int*[grafo->numNodes];
    for(int i = 0; i < grafo->numNodes; i++)
        freq[i] = new int[grafo->numNodes];

    p0 = 1.0 / grafo->numNodes;
    saiu = new bool[grafo->numNodes];
    pAcu = new double[grafo->numNodes];
    rmap = new int[grafo->numNodes];

    int i = {0};
    std::generate(map, map+grafo->numNodes, [&i]{ return i++; });

    cout << "Build AED finish" << endl;
}

int AED::solve(int output[], int popSize, int eliteSize, int iter, double alpha){
    cout << "Solve AED starting" << endl;

    int* tagaux = new int[grafo->numNodes];

    Solution working(tagaux,0, grafo->numNodes);
    for(int i = 0; i < grafo->numNodes; i++)
    {
        double s = 0;
        for(int j = 0; j < grafo->numNodes; j++)
        {
            p[i][j] = p0;
            s += p[i][j];
        }
        for(int j = 0; j < grafo->numNodes; j++)
        {
            p[i][j] /= s;
        }
    }

    int bestCost = INT_MAX;
    elite.clear();

    for(int k = 0; k < iter; k++)
    {
        for(int i = 0; i < popSize; i++)
        {
            Solution s = sample(working);
            s.cost = climber->climb(s.tag, s.cost);
            elite.push_back(s);
            elite.sort([](Solution a, Solution b)->bool{ return a.cost > b.cost;});

            if(elite.size() > eliteSize)
            {
                working = elite.front();
                elite.pop_front();
            }

            if(s.cost < bestCost)
            {
                bestCost = s.cost;
                std::copy(s.tag, s.tag + grafo->numNodes, output); //copy tag to output
            }
        }

        for(int i = 0; i < grafo->numNodes; i++)
        {
            std::fill_n(freq[i], grafo->numNodes, 0);
        }


        for(Solution s : elite)
        {
            for(int i = 0; i < grafo->numNodes; i++)
            {
                freq[i][s.tag[i]]++;
            }
        }

        for(int i = 0; i < grafo->numNodes; i++)
        {
            for(int j = 0; j < grafo->numNodes; j++)
            {
                p[i][j] = (1 - alpha) * p[i][j] + alpha*freq[i][j]/eliteSize;
            }
        }
    }

    cout << "Solve AED finished" << endl;

    return bestCost;
}

Solution AED::sample(Solution& s){

    random_shuffle(map, map + grafo->numNodes);
    fill_n(saiu, grafo->numNodes, false);

    for(int ii = 0; ii < grafo->numNodes; ii++)
    {
        int i = map[ii];
        s.tag[i] = roletaNormalizada(i, saiu);
        saiu[s.tag[i]] = true;
    }

    s.cost = grafo->cost(s.tag);

    return s;

}

int AED::roletaNormalizada(int v, bool saiuRotulo[]){
    int size = 0;
    fill_n(pAcu, grafo->numNodes, 0 );

    for(int r = 0; r < grafo->numNodes; r++)
    {
        if(!saiuRotulo[r])
        {
            pAcu[size] = p[v][r];
            rmap[size++] = r;
        }
    }

    for(int i = 1; i < size; i++)
    {
        pAcu[i] += pAcu[i-1];
    }

    for(int i = 0; i < size; i++)
    {
        pAcu[i] /= pAcu[size - 1];
    }

    double x = (rand() % 101) / 100;
    for(int i = 0; i < size; i++)
    {
        if(pAcu[i] >= x)
        {
            return rmap[i];
        }
    }

    return rmap[size - 1];
}




