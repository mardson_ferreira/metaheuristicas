#include "ga.h"

GA::GA(Grafo *grafo, int popSize):Heuristic(grafo)
{
    used = new bool[grafo->numNodes];
    filho = new int[grafo->numNodes];
    verticeA = new int[grafo->numNodes];
    verticeB = new int[grafo->numNodes];
    tag = new int[grafo->numNodes];

    popLength = popSize;
    pop = new Solution[popSize];

    int i = {0};
    std::generate(tag, tag + grafo->numNodes, [&i]{ return i++; });
}

bool randomBoolean() {
  return rand() % 2 == 1;
}


float nextFloat(){
    return (rand() % 101) / 100; //retorna um float entre 0.0 e 1.0
}

int GA::solve(int ite, int out[], float mutateRate, Solver *solver){
    for(int i = 0; i < popLength; i++)
    {
        int cost = solver->solve(tag);
        Solution s(tag, cost, grafo->numNodes);
        pop[i] = s;
    }

    for(int i = 0; i < ite; i++)
    {
        sort(pop, pop + popLength);
        Solution a = tournamentSelect(10);
        Solution b = tournamentSelect(10);

        if(a != b)
        {
            Solution s = cross(a,b);
            if( s != a && s != b){
                pop[popLength - 1] = s;
            }
        }

        if(nextFloat() < mutateRate)
        {
            sort(pop, pop + popLength);
            Solution s = tournamentSelect(10);
            s = mutateFlip2(a,1);
            pop[popLength - 1] = s;
        }
    }

    std::copy(pop[0].tag, pop[0].tag+pop[0].size, out); //copy pop[0].tag to out

    return pop[0].cost;
}

Solution GA::mutateFlip2(Solution s, int gap){
    int i,j;
    i = rand() % s.size;
    j = rand() % s.size;
    while (i == j || gap < abs(s.tag[i] - s.tag[j])) {
        j++;
        if(j == s.size)
        {
            j = 0;
        }
    }
    int* t = new int[s.size];
    std::copy(s.tag, s.tag + s.size, t); //copy s.tag to t
    std::swap(t[i], t[j]);

    Solution r(t, grafo->cost(t), s.size);

    return r;
}

Solution GA::tournamentSelect(int k){
    Solution s = pop[rand() % popLength];
    for(int i = 0; i < k; i++)
    {
        int x = rand() % popLength;
        if(s.cost > pop[x].cost)
        {
            s = pop[x];
        }
    }

    return s;
}

Solution GA::cross(Solution a, Solution b)
{
    if(randomBoolean())
    {
        std::swap(a,b);
    }

    int cut = grafo->numNodes / 4 + (rand() % (grafo->numNodes / 2));
    a.vertice(verticeA);
    b.vertice(verticeB);
    std::fill_n(filho, grafo->numNodes, -1);
    std::fill_n(used, grafo->numNodes, false);
    for(int i = 0; i < cut; i++)
    {
        filho[i] = verticeA[i];
        used[filho[i]] = true;
    }

    for(int i = cut; i < grafo->numNodes; i++)
    {
        if(!used[verticeB[i]])
        {
            filho[i]= verticeB[i];
            used[filho[i]] = true;
        }
    }

    for(int i = 0; i < grafo->numNodes; i++)
    {
        if(filho[i] == -1)
        {
            for( int j = 0 ; j < grafo->numNodes; j++)
            {
                if(!used[verticeB[j]])
                {
                    filho[i] = verticeB[j];
                    used[filho[i]] = true;
                    j++;
                    break;
                }
            }
        }
    }

    for(int i = 0; i < grafo->numNodes; i++)
    {
        tag[filho[i]] = i;
    }

    Solution s(tag,grafo->cost(tag), grafo->numNodes);

    return s;

}
