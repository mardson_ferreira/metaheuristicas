This project contains well-know heuristics for the minimum linear arrangement problem (MinLA). MinLA consists in finding a labeling of the vertices of $G$ such that the sum of the weights of its edges is minimized. 

https://www.sciencedirect.com/science/article/abs/pii/S1571065317302500?via%3Dihub