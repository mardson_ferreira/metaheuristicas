#include "ils.h"

ILS::ILS(Grafo *grafo, Solver *solver, Climber *climber):Heuristic(grafo), solver(solver), climber(climber)
{
    current = new int[grafo->numNodes];
    map = new int[grafo->numNodes];

    int i = {0};
    std::generate(map, map+grafo->numNodes, [&i]{ return i++; }); //inicializa map com valores de 0 até g.N-1

}

int ILS::solve(int best[], int flipSize, int tryouts, float alpha){
    bool flag = false;
    if (verbose) {
        cout << "### Iterated Local Search started ###" << endl;
    }

    int* neighbor = new int[grafo->numNodes];
    std::copy(best, best + grafo->numNodes, neighbor); //copy best to neighbor
    int currentCost = climber->climb(current, solver->solve(current));
    int neighborCost;
    int bestCost = currentCost;
    int count = 0;

    do {
        std::copy(current, current + grafo->numNodes, neighbor); //copy current to neighbor
        neighborCost = climber->climb(neighbor, pickAFlip(flipSize, neighbor, currentCost));
        if(neighborCost < currentCost){
            std::copy(neighbor, neighbor + grafo->numNodes, current); //copy neighbor to current
            currentCost = neighborCost;
            if(neighborCost < bestCost){
                std::copy(neighbor, neighbor + grafo->numNodes, best); //copy neighbor to best
                bestCost = neighborCost;
                if(verbose){
                    cout << "count: " << count << " cost: " << bestCost << endl;
                }
                count = 0;
                flag = true;
            }
        }else if(neighborCost < currentCost * alpha){
            std::copy(neighbor, neighbor + grafo->numNodes, current); //copy neighbor to current
            currentCost = neighborCost;
        }
        count++;

    } while( count < tryouts );

    if(!flag){
       std::copy(neighbor, neighbor + grafo->numNodes, best); //copy neighbor to current
       bestCost = neighborCost;
    }

    if(verbose){
        cout << "ILS done" << endl;
    }


    return bestCost;

}


int ILS::pickAFlip(const int n, int tag[], const int cost){
    random_shuffle(map, map+grafo->numNodes);
    srand (time(NULL));

    int delta = grafo->subCost(tag, map, n);

    for(int i = n-1; i > 0; i--){
        int nextInt = rand() % i; // range 0 to  i
        Grafo::flip(map[i], map[nextInt], tag);
    }

    delta = grafo->subCost(tag, map, n) - delta;

    return cost + delta;
}
