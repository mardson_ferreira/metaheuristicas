#ifndef ILS_H
#define ILS_H

#include "heuristic.h"
#include "grafo.h"
#include "solver.h"
#include "climber.h"

class ILS : public Heuristic
{

public:
    ILS(Grafo* grafo, Solver* solver, Climber* climber);
    int solve(int best[], int flipSize, int tryouts, float alpha);

private:
    Solver* solver;
    Climber* climber;
    int* current;
    int* map;
    bool verbose = true;

    int pickAFlip(const int n, int tag[], const int cost);
};

#endif // ILS_H
