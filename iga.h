#ifndef IGA_H
#define IGA_H

#include "solution.h"

class Selector {
public:
    virtual Solution select(Solution pop[]) = 0;
};

class Mutation {
public:
    virtual Solution mutate(Solution s) = 0;
};

class Cross {
public:
    virtual Solution cross(Solution a, Solution b) = 0;
};



#endif // IGA_H
