#ifndef TABULIST_H
#define TABULIST_H

#include <iostream>
#include "list"
#include "solution.h"

using namespace std;

class TabuList
{
public:
    TabuList(int t, int size_sol);
    void add(int tag[], int cost);
    bool isTabu(int cost, int tag[]);
    void clear();
    int size();
    int lenghtSolution();

private:
    list<Solution> lista;
    int lenght_sol, tenure;

};

#endif // TABULIST_H
