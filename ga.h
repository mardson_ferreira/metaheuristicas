#ifndef GA_H
#define GA_H

#include "heuristic.h"
#include "grafo.h"
#include "solution.h"
#include "vector"
#include "iga.h"
#include "solver.h"

class GA : public Heuristic
{
public:
    GA(Grafo* grafo, int popSize);
    int solve(int ite, int out[], float mutateRate, Solver* solver);


private:
    Solution* pop;
    int popLength;
    int *filho, *verticeA, *verticeB, *tag;
    bool *used;

    Solution cross(Solution a, Solution b);
    Solution tournamentSelect(int k);
    Solution mutateFlip2(Solution s, int gap);
};

#endif // GA_H
