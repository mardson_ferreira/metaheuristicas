#include "vnd.h"

VND::VND(Grafo *grafo, vector<lsmethod>& neighMethods):Heuristic(grafo), lsNeighbors(neighMethods){
}

int VND::climb(int output[], int custo){

    if(verbose){
        if(firstImp){
            cout << "### VND (first Improve) started###" << endl;
        }else{
            cout << "### VND started ###" << endl;
        }
    }

    int c;
    int nc = custo;
    do{
        int i = 0;
        do{
            c = nc;
            nc = lsNeighbors[i++](output, nc, firstImp); // chamando ls[i].flip_i
        }while(i < lsNeighbors.size() && nc >= c);
        if(verbose){
            cout << "cost:" << nc << endl;
        }
    }while(nc < c);

    if(verbose){
        cout << "### VND Done ###" << endl;
    }

    return nc;

}
