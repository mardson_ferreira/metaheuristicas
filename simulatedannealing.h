#ifndef SIMULATEDANNEALING_H
#define SIMULATEDANNEALING_H

#include "heuristic.h"
#include "grafo.h"
#include "solver.h"


class SimulatedAnnealing : public Heuristic
{

public:
    SimulatedAnnealing(Grafo* grafo, Solver* sol);
    int solve(int ite, int best[], double tIni, int maxGap);

private:
    int* map;
    int *currente, *neighbor;
    Solver* solver;
    bool verbose = true;
    int pickAFlip2(int tag[], int custo, int maxGap);

};

#endif // SIMULATEDANNEALING_H
