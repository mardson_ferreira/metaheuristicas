#include "grasp.h"

GRASP::GRASP(Grafo *grafo, int tol, bool firstImpr, Climber *climber):Heuristic(grafo),
    fip(firstImpr), ms(grafo, firstImpr), gag(grafo,tol), vnd(climber)
{
    tag = new int[grafo->numNodes];
}


int GRASP::solve(int iter, int output[]){
    if(verbose){
        cout << " ### GRASP started ### " << endl;
    }

    int bestCost = ms.solve(iter, output, vnd, &gag);

    if(verbose){
        cout << " ### GRASP Done ### " << endl;
    }

    return bestCost;
}
