#include "simulatedannealing.h"

SimulatedAnnealing::SimulatedAnnealing(Grafo *grafo, Solver *sol):Heuristic(grafo), solver(sol)
{
    currente = new int[grafo->numNodes];
    neighbor = new int[grafo->numNodes];
    map = new int[grafo->numNodes];

    int i = {0};
    std::generate(map, map+grafo->numNodes, [&i]{ return i++; }); //inicializa map com valores de 0 até g.N-1

}

int SimulatedAnnealing::pickAFlip2(int tag[], int custo, int maxGap){
    random_shuffle(map, map+grafo->numNodes);

    for(int i = 1; i < grafo->numNodes; i++){
        int tag0 = tag[map[0]];
        if( abs(tag0 - tag[map[i]]) <= maxGap){
            int aux = map[i];
            map[i] = map[1];
            map[1] = aux;
            break;
        }
    }

    int delta = grafo->flipDelta(map[0], map[1], tag);
    Grafo::flip(map[0], map[1], tag);

    return custo + delta;
}


float acceptance_probability(float old_cost, float new_cost, float T){
        return exp((old_cost - new_cost) / T);
}

double binaryRandom(){
    srand (time(NULL));
    return (double) rand() / (double) (RAND_MAX) ;
}

int SimulatedAnnealing::solve(int ite, int best[], double tIni, int maxGap){
    if(verbose){
        cout << "### Simulated Annealing started ###" << endl;
    }

    int currCost = solver->solve(currente); //verificar possível erro depois
    int bestCost = currCost;
    std::copy(currente, currente+grafo->numNodes, best);

    for(int i = 0; i < ite; i++){
        double t = tIni * (ite - i) / ite;
        std::copy(currente, currente+grafo->numNodes, neighbor);
        int neighCost = pickAFlip2(neighbor, currCost, maxGap); //busca nova vizinhança
        int delta = neighCost - currCost;
        if(delta < 0){
            std::copy(neighbor, neighbor+grafo->numNodes, currente); //atualiza a solução corrente
            currCost = neighCost; //atualiza o custo da solução corrente
            if(neighCost < bestCost){
                std::copy(neighbor, neighbor+grafo->numNodes, best); //atualiza a melhor solução
                bestCost = neighCost; //atualiza o valor da melhor solução
                if(verbose){
                    cout << "COST: " << bestCost << endl;
                }
            }
        }else if(acceptance_probability(currCost, neighCost, t) > binaryRandom()){
            std::copy(neighbor, neighbor+grafo->numNodes, currente);
            currCost = neighCost;
        }
    }

    if (verbose) {
         cout << "### Simulated Annealing done ###" << endl;
    }

    return bestCost;
}
