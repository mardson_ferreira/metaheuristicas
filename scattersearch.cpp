#include "scattersearch.h"

ScatterSearch::ScatterSearch(Grafo* g, Solver *s, Climber *c):Heuristic(g), solver(s), climber(c)
{
    map = new int[grafo->numNodes];
    used = new bool[grafo->numNodes];
    filho = new int[grafo->numNodes];
    verticeA = new int[grafo->numNodes];
    verticeB = new int[grafo->numNodes];
    tag = new int[grafo->numNodes];

    int i = {0};
    std::generate(map, map+grafo->numNodes, [&i]{ return i++; }); //inicializa map com valores de 0 até g.N-1

    random_shuffle(map, map + grafo->numNodes);

}

bool randomBool() {
  return rand() % 2 == 1;
}


Solution* ScatterSearch::scatterSet(int popSize, int refSize){
    Solution* pop = new Solution[popSize];
    for(int i = 0; i < popSize; i++ )
    {
        int* tag = new int[grafo->numNodes];
        int cost = solver->solve(tag);
        pop[i] = Solution(tag, cost, grafo->numNodes);
        delete []tag;
    }

    if(verbose){
        for(int k = 0; k < popSize; k++){
            cout << "pop[" << k << "].cost = " << pop[k].cost << endl;
            cout << "pop[" << k << "].tag= [";
                 for(int p =0 ; p < grafo->numNodes; p++)
                    cout << " " << pop[k].tag[p];
            cout << "]" << endl;
        }
        cout << "\n";
    }

    int max = 0, argI =-1, argJ = -1;

    for(int i = 0; i < popSize; i++)
    {
        for(int j = i + 1; j < popSize; j++)
        {
            int d = pop[i].distanceTo(pop[j]);
            if(d > max)
            {
                max = d;
                argI = i;
                argJ = j;
            }
        }
    }

    Solution* ref = new Solution[refSize];
    ref[0] = pop[argI];
    int* dist =  new int[popSize];
    std::fill_n(dist, popSize, INT_MAX);
    Solution r = ref[0];

    for(int j = 1; j < refSize; j++)
    {
        max = 0;
        argI = -1;
        for(int i = 0; i < popSize; i++)
        {
            dist[i] = std::min( r.distanceTo(pop[i]), dist[i]);
            if(max < dist[i])
            {
                max = dist[i];
                argI = i;
            }
        }
        r = ref[j] = pop[argI];
    }

    return ref;
}


Solution ScatterSearch::cross(Solution a, Solution b)
{
    if(randomBool())
    {
        std::swap(a,b);
    }

    int cut = grafo->numNodes / 4 + (rand() % (grafo->numNodes / 2));
    a.vertice(verticeA);
    b.vertice(verticeB);
    std::fill_n(filho, grafo->numNodes, -1);
    std::fill_n(used, grafo->numNodes, false);
    for(int i = 0; i < cut; i++)
    {
        filho[i] = verticeA[i];
        used[filho[i]] = true;
    }

    for(int i = cut; i < grafo->numNodes; i++)
    {
        if(!used[verticeB[i]])
        {
            filho[i]= verticeB[i];
            used[filho[i]] = true;
        }
    }

    for(int i = 0; i < grafo->numNodes; i++)
    {
        if(filho[i] == -1)
        {
            for( int j = 0 ; j < grafo->numNodes; j++)
            {
                if(!used[verticeB[j]])
                {
                    filho[i] = verticeB[j];
                    used[filho[i]] = true;
                    j++;
                    break;
                }
            }
        }
    }

    for(int i = 0; i < grafo->numNodes; i++)
    {
        tag[filho[i]] = i;
    }

    Solution s(tag,grafo->cost(tag), grafo->numNodes);

    return s;

}

bool equals(int a[], int b[], int size){
    return std::equal(a, a+size, b, []( int i, int j)->bool{return i == j;} );
}


Solution ScatterSearch::pathRelink(Solution sol1, Solution sol2){
    int *s1, *s2, *s, *best;
    s1 = new int[grafo->numNodes];
    s2 = new int[grafo->numNodes];
    s = new int[grafo->numNodes];
    best = new int[grafo->numNodes];

    std::copy(sol1.tag, sol1.tag + grafo->numNodes, s1); //copy sol1.tag to s1
    std::copy(sol2.tag, sol2.tag + grafo->numNodes, s2); //copy sol2.tag to s2

    std::copy(s1, s1 + grafo->numNodes, s); //copy s1 to s
    std::copy(s1, s1 + grafo->numNodes, best); //copy s1 to best

    int bestCost = INT_MAX;

    for(int i = 0; i < grafo->numNodes; i++)
    {
        int min = grafo->numNodes;
        int minArg = -1;
        for(int j = 0; j < grafo->numNodes; j++)
        {
            if( s[j] != s2[j] && abs(s[j] - s2[j]) < min )
            {
                min = abs(s[j] - s2[j]);
                minArg = j;
            }
        }
        if( minArg >= 0)
        {
            int k = 0;
            for( ; k < grafo->numNodes; k++)
                if(s[k] == s2[minArg])
                    break;

            std::swap(s[minArg], s[k]);
            int custo = grafo->cost(s);

            if( custo < bestCost && (custo != sol2.cost || !equals(s, s2, grafo->numNodes)))
            {
                bestCost = custo;
                std::copy(s, s+grafo->numNodes, best); //copy s to best
            }

        }else {
            break;
        }

    }

    if(bestCost == INT_MAX)
        bestCost = sol1.cost;

    Solution solution(best, bestCost, grafo->numNodes);

    return solution;

}


int ScatterSearch::solve(int best[], int popSize, int refSize,int tryouts){
    Solution* ref = scatterSet(popSize, refSize);
    std::sort(ref, ref+refSize); // usa a sobrescrita do operador < para comparar

    for(int t = 0; t < tryouts; t++)
    {
        for(int i = 0; i < refSize; i++)
        {
           for(int j = i+1; j < refSize; j++)
           {
               Solution s = pathRelink(ref[i], ref[j]);
               if(!Grafo::IsPermutation(s))
               {
                    return -1;
               }
               s.cost = climber->climb(s.tag, s.cost);
               if( s.cost < ref[ refSize - 1 ].cost)
               {
                    bool novo = true;
                    for(int k = 0; k < refSize ; k++){
                        if(ref[k] == s)
                        {
                            novo = false;
                            break;
                        }
                    }
                    if(novo)
                    {
                        ref[ refSize - 1 ] = s;
                        std::sort(ref, ref + refSize);
                        break;
                    }
               }

           }
        }
    }

    std::copy(ref[0].tag, ref[0].tag + ref[0].size, best); //copy ref[0] to best

    return ref[0].cost;
}
